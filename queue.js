

let collection = [];

// Write the queue functions below.
// (1) Make sure to have correct function names
function print(){
	return collection;
}

function enqueue(element){
	collection[collection.length] = element;
	return collection;
}

function dequeue() {
    for (let i=1; i < collection.length; i++){ 
	    collection[i - 1] = collection[i];
	    collection.length--;
    }
    return collection;
}

function size() {
    return collection.length;
}

function front() {
    return collection[0];
}


function isEmpty() {

    if (collection.length == 0){
        return collection;
    } 
    else {
        return false;
    }
}


// (2) Make sure to include the functions in exportation
module.exports = {
	print, enqueue, dequeue, front, size, isEmpty
};